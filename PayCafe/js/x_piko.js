/////////////////
// piko ////////
///////////////

$.pikoVersion	=	0.52;











// GLOBAL VARIABLES

var weekday 	= ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
var months 		= ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var monthsB 	= ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
$.stor			= $.rdom + '.' + $.app + ".";															// make the storage spot name (rdom + app id)
$.ls 			= window.localStorage;																	// make an easy alias for the localstorage option






// FUNDAMENTAL FUNCTIONS (ONE-LINERS)

$.ajaxSetup ({ cache: false });																			// no ajax cache please!
function pad(n) { return (n < 10) ? '0' + n : n; }														// for padding zeroes
function setLS(spot,val) { $.ls[$.stor+spot] = val; }													// function to set a local storage var
function getLS(spot) { return $.ls[$.stor+spot]; }														// function to fetch a local storage var
function clearLS(spot) { localStorage.removeItem($.stor+spot); }										// function to remove a local storage var entirely
function spit(msg) { if ($.spit) { console.log(msg); } }												// function to spit results to the console on command
function getRand (min, max) { return Math.floor(Math.random() * (max - min + 1)) + min; }				// function to get random number between two poles
function shareMe(text) { $('.fadeout').fadeIn(); $('.share').attr('add', text).fadeIn(); }				// triggered when you click something to share
function toRad(v) { return v * Math.PI / 180; }															// convert a number to a radical. I mean radial.
String.prototype.splice = function( idx, rem, s ) { return (this.slice(0,idx) + s + this.slice(idx + Math.abs(rem))); }	// splice-o-rama!
Object.size = function(obj) { var size = 0, key; for (key in obj) { if (obj.hasOwnProperty(key)) size++; } return size; };






// DEVICE INIT
// This runs first, before jQuery, before anything else. This controls the ENTIRE UNIVERSE!
// Not really.

if ($.testOnWeb != 1 && navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|iOS)/)) {		// detect the type of load... app or browser

	spit("Running as an app");
	
	$.getScript("cordova.js", function() {																// if app, load cordova.js
	    window.phonegap = true;																			// define this as a phonegap app
	    $.each($.cordovaJS,function(p,v) {																// for each of the cordova JS files we define at the start...
	    	spit(v);																					// tell us we're loading it
			$.getScript('js/'+v+'.js'); 																// load the JS
	    });
	});

	document.addEventListener("deviceready", startUp, false);											// listen for "deviceready", and do startUp() when you hear it
	
} else {

	spit("Running in a browser");

	$(document).ready(function() {																		// browsers just wait for jQuery's document.ready()
		startUp();																						// then startUp()
	});
	
}





// APP INIT
// This runs once the device (or jQuery) is ready. It sets up the basic parameters so we can actually do stuff.

function startUp() {																					// one function to rule them all...

	spit($.app + ' ' + $.version + ' running Piko ' + $.pikoVersion + ' on jQuery ' + $().jquery);
	$(document).attr("title", $.app);

																										// if the app requires geolocation, we need to get the coordinates now
	if (navigator.geolocation && $.needGeo > 0) {
		checkLocation($.needGeo);
	}

	$('body').append('<div class="page">');																// add the original page element

																										// add the spinner (throbber)
	$('body').append('<div class="spinnerContainer"><div class="spinner"><div class="bar1"></div><div class="bar2"></div><div class="bar3"></div><div class="bar4"></div><div class="bar5"></div><div class="bar6"></div><div class="bar7"></div><div class="bar8"></div><div class="bar9"></div><div class="bar10"></div><div class="bar11"></div><div class="bar12"></div></div></div>');
		
																										// add the sharing popup
	$('body').append('<div class="fadeout"></div><div class="popup share"><a class="fb" native="fb://publish?u=" go="http://www.facebook.com/sharer/sharer.php?u="></a>	<a class="tw" native="twitter://post?message=" go="http://www.twitter.com/home?status="></a></div>');

																										// add the search box
	$('body').append('<div class="searchbar"><input id="searchthis" placeholder="Search" /><div class="search_tweak"></div></div>');



	$.fp 	= qs(".page");																				// check the url for the page to load...
	loadPage($.fp);																						// load whatever page is requested	

	$('body').on("click","a, btn",function(e) {															// when someone clicks on an anchor tag, we interrupt...

																										// if the target is NOT _blank or _system, we proceed...
		if ($(this).attr('target')!=="_blank" && $(this).attr('target')!=="_system" && $(this).attr('href')) { 	
			e.preventDefault();																			// stop it from doing its default
			e.stopPropagation();																		// kill all bubbles
			
			var element = $(this);																		// define the element as the object clicked on
			$(element[0].attributes).each(function(p,v) {												// drop all its attributes to LS
				setLS(this.nodeName,this.nodeValue);													// each and every one
			});
			
			$('.spinnerContainer').fadeIn(100);															// fade in the spinner (loading thing)
			
			if ($(this).attr('move') != "popover") {
				window.history.pushState(null, null, 'index.html?page='+$(this).attr('href'));			// change the URL and browser history
			}
			loadPage($(this).attr('href'),$(this).attr('move'));										// "load" the URL in question
		}
	})
	
	.on("click", ".fadeout",function() {																// if we ever click on the faded-out background
		$(this).fadeOut();																				// hide it
		$('.popup').fadeOut();																			// and the popup in front of it
		$('.popover').fadeOut(300, function() {$('.popover').remove()});																			// and the popup in front of it

	})
	
	.on("click", ".share a", function() {																// if we click on a share icon...					
		$(this).parent().fadeOut();																		// hide the share screen
		$('.fadeout').fadeOut();																		// and the fadeout
		var add = $(this).parent().attr('add');															// the social container has the added content in its "add" attr
		document.location = $(this).attr('native')+encodeURI(add);										// first off: try to open the native URL first
		setTimeout( function() {																		// but in the meantime, try a 300ms timeout
			var s = window.open($(this).attr('go')+encodeURI(add),'_system');							// if the native one fails, try the web version instead
		}, 300);

	});
	
	$(function() {
	    FastClick.attach(document.body);
	});

}




// LOADING PAGES 
// This converts one colour into another.
// Honestly. It's called loadPage. What did you expect?

function loadPage(where,move) {																			// loads a new page
	
	spit("Loading " + where);
	$.timeMe = new Date().getTime();
	
	$.thePage	= '';																					// blank out the requested page contents
	$.fp 		= "pages/" + where;																		// prep file loc
	$.w 		= where.replace(".html","");
	$.move 		= move; 

	firstPrep();

	$.get($.fp,function(d) {																			// template!
		$.thePage = d;	
		var script = $(d).get(0).outerHTML;		
		$('body').append('<div class="page ' + $.w + '">' + script + '<content></content></div>');		// add a new page to the page, with the guts we've defined already																
	});

}



// MOVING PAGES
// This moves the old page offscreen. We divide this in half for kicks. Just play along.

function movePage(st) {																					// for building the first bits of the page

	if (st == undefined) {																				// st means transition (for some reason)
		st = $.firstTrans;																				// if it's undefined, set it to the default (named up at the top)
	}

	$('.page').eq(1).html($($.thePage)[2]);
	
	if (st == "pop") {																					// if this page is "popping" on...
		$('.page').eq(0).hide();																		// hide the old page
		moveIn(st);																						// and do the next step
		
	} else if (st == "fade") {																			// if this page is fading on...
		$('.page').eq(0).fadeOut(300,function() {														// fade out the old page, and when done...
			moveIn(st);																					// do the next step
		});
		
	} else if (st == "slide") {																			// if this page is sliding on...
		$('.page').eq(0).animate({'left':$(window).width()*-1}, 300);									// slide off the old page, and at the same time...
		moveIn(st);																						// do the next step!
		
	} else if (st == "slideR") {																		// if we're sliding the other way...
		$('.page').eq(0).animate({'left':$(window).width()}, 300);										// slide off the old page...
		moveIn(st);																						// you guessed it!
	}
	
	else if (st == "popover") {
		moveIn(st);
	}
	
	lastPrep();																							// do the "last prep" stuff		
}



// MOVING MORE PAGES
// This moves the new pages onscreen, once we've done everything else.

function moveIn(st) {																					// for moving in new pages

	var tp = $('.page').eq(1);																			// we're talking about the NEW page here...
	tp.css({'width':$(window).width(), 'height':$(window).height(),'left':$(window).width()});			// so size it to the window, and move it off the right of the screen

	if (st == "pop") {																					// if we're popping...
		tp.css({'left':0});																				// just move it onscreen at once
		closeUp();																						// and close up
		
	} else if (st == "fade") {																			// if we're fading it in...
		tp.hide().css({'left':0}).fadeIn(300, function() { closeUp(); });								// hide it, move it left, and fade it in... then close up
		
	} else if (st == "slide") {																			// if we're sliding it in...
		tp.animate({'left':0},300, function() { closeUp(); });											// animate it in, then close up.
		
	} else if (st == "slideR") {																		// if we're sliding the other way...
		tp.css({'left':$(window).width()*-1}).animate({'left':0},300, function() { closeUp(); });		// animate it in, then close up
	
	} else if (st == "popover") {
		$('.fadeout').fadeIn(300);
		tp.addClass('popover').css({'left':15, 'top':(($(window).height()-150)/2), 'right':15 ,'width':'auto', 'height':150}).fadeIn(300);
		$('.spinnerContainer').fadeOut(100); 																// hide the spinner
	}
	
	
}



// CLOSING UP
// This viciously assassinates the first page. With a vengeance. Bruce Willis is involved. The 12 Monkeys one, though, not Die Hard.

function closeUp() {
	spit("--- finished in " + (new Date().getTime() - $.timeMe) + "ms");
	$('.page').eq(0).remove(); 
	$('.spinnerContainer').fadeOut(100); 
}



// BUILDING PAGES
// This is where the magic is. This requests content (for a page... which is really a misnomer... hmmm... one sec...)
// So honestly, this isn't building a page at all. This is requesting data to populate a page. 
// So I should probably rename it some day. But not today, Galvatron!
// So.
// As you were.

function buildPage(myspot,fetch,callback) {																// get the data required to build the page

	if ($.killCache == 1 || verifyTimeDelay(myspot+'.time')) {											// if the cache says this page can be updated...
		spit("Getting data from " + fetch);
		$.getJSON(fetch, function(cx) {																	// get the data we're after via JSON
			setLS(myspot,JSON.stringify(cx));															// store it in LS
			setLS(myspot+'.time',new Date().getTime());													// and set the cache time to now
			callback(cx);																				// process whatever callback was defined
		})
		.fail(function() {																				// if something goes wrong...
			spit("Data load failed. Using cache instead");
			if (getLS(myspot)) {																		// and we can find a cached version...
				var cx = JSON.parse(getLS(myspot));														// prep that cached version into an Object
			} else {																					// if we CAN'T even find cache
				var cx = {"posts":{}};																	// make an empty Object so we can at least pretend to proceed
			}
			callback(cx);																				// process the callback
		});
	} else {																							// HOWEVER, if we are still within the cache period
		spit("Cache still good!");
		var cx = JSON.parse(getLS(myspot));																// parse the cache into an Object
		callback(cx);																					// and start makin' that page
	}	
}



// FUNDAMENTAL FUNCTIONS THAT ARE VERY IMPORTANT BUT NOT VERY FUN


function qs(snatch) {																					// snag things from the URL
	var a = window.location.search.substr(1).split('&');												// split the query string
    if (a == "") { return "splash.html"; }																// if the query string is empty, return "splash.html" (home)
    var b = {}, i = 0;																					// prepare for adventure!
    while (i < a.length) {																				// traverse the array
        var p=a[i].split('=');																			// split the bit
        if (p.length === 2) {																			// if there are two sides to it..
	        b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));										// get the one in question
	    }
        i += 1;
    }
    if (b.page === "undefined") { b.page = "splash.html"; }												// if we still don't know what "page" is...
    return b.page;																						// return it!
}


function showError(head, msg, btns, func) {																// if there's an error, show it here
	if (navigator.notification) {																		// if we CAN show an error, do it...
		navigator.notification.vibrate(500);
		navigator.notification.confirm(
			msg,																						// show the msg
			function(buttonIndex) { if(func) { 															// if there is a callback set
				window[func](buttonIndex); 																// do that callback
			} },
			head,																						// errors have headers
			btns																						// and a button array
		);
	} else {
		spit(msg);																						// no errors? show the msg in console
		if (func) { window[func](1); }																	// and pretend we hit the righthand button
	}
}


function rp(ar) {																						// replacing the contents of a string
	if ($.thePage) {																					// apply to the current page
		$.each(ar, function(p,v) {
			var d, dx, fl, pz;
			if (p.substr(0,1)==="/") {																	// if we're doing conditional tags
				p = p.substr(1);
				d = new RegExp('{{'+p+'}}[\s|\S]*?(.*?)[\s|\S]*?{{\/'+p+'}}', 'ig');
				$.thePage = $.thePage.replace(d, v);
			} else {																					// otherwise just replace the guts
				d = new RegExp('{{'+p+'}}', 'ig');
				$.thePage = $.thePage.replace(d, v);
				fl = String(v).toLowerCase().substr(0,1);
				dx = new RegExp('{{a\\-'+p+'}}', 'ig');
				if (fl==="a" || fl==="e" || fl==="i" || fl==="o" || fl==="u") {							// fix some grammar ("aN orchestra" vs "a orchestra")
					v = "an "+v;
				} else {
					v = "a "+v;
				}
				$.thePage = $.thePage.replace(dx, v);													// dump the new page back to the var
			}
		});
	}
}


function repAll() {																						// wipe out leftover dynamic spots
	if ($.thePage) { $.thePage = $.thePage.replace(/\{\{(\S+)\}\}/g, ''); }								// kill 'em all!
}


function verifyTimeDelay(check,ex) {																	// checking if we should use cache or not
	if (!ex) { var ex = $.Pcache; }																		// if there's no cache timeout set, grab the default
	if (getLS(check) === undefined) {																	// if there IS no cache in that spot, we need it
		return true;																					// so say true
	} else if (getLS(check)+(ex*3600000) > new Date().getTime()) {										// if the expiry is newer than right now...
		return false;																					// then no, we're good
	}
}


function shuffle(a) {																					// shuffle an array
      var i = a.length - 1;
      var j, temp;
      while (i > 0) {
            j = Math.floor(Math.random() * (i + 1));
            temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            i = i - 1;
      }
      return a;
}



// Check location once
function checkLocation(freq, trigger) {

	if (freq === 1) {
		navigator.geolocation.getCurrentPosition(function(position) {
			$.myLat = position.coords.latitude;
			$.myLon = position.coords.longitude;
			if (trigger) {
				trigger();
			}
		}, function(error) {
			lg(error);
		}, {timeout:10000});
	} 
	
	else if (freq === 2) {
		navigator.geolocation.watchPosition(function(position) { 
			$.myLat = position.coords.latitude;
			$.myLon = position.coords.longitude;
			if (trigger) {
				trigger();
			}
		}, function(error) {
			lg(error);
		}, {timeout:10000});
	}
}

// Find the distance between two points
function distances(lat1,lon1) {

	// if we don't have a $.mylat (my current location), this won't work
	if ($.myLat === undefined) {
		console.log("booo");
		return 0;
	}

	var lat2 = $.myLat;
	var lon2 = $.myLon;	

	var R = 6371; // km
	var dLat = toRad(lat2-lat1);
	var dLon = toRad(lon2-lon1);
	lat1 = toRad(lat1);
	lat2 = toRad(lat2);
	
	// here by voodoo
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	var d = R * c;

	return d;
}




/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};



if ('ontouchstart' in document.documentElement) {
    $(document).on('touchstart', 'a', function (e) {
        var self = this;
        this.timeout = setTimeout(function () {
            $(self).addClass('active');
        }, 50);
    }).on('touchmove', 'a', function (e) {
        clearTimeout(this.timeout);
        $(this).removeClass('active');
    }).on('touchend', 'a', function (e) {
        clearTimeout(this.timeout);
        var self = this;
        this.timeout = setTimeout(function () {
            $(self).removeClass('active');
        }, 200);
    });
// Desktop
} else {
    $(document).on('mousedown', 'a', function (e) {
        $(this).addClass('active');
        var self = this;
        function mouseup () {
            $(document).off('mouseup', mouseup);
            $(self).removeClass('active');
        };
        $(document).on('mouseup', mouseup);
    });
}
