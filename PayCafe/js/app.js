///////////////////////////////////////////////////
// PIKO LIGHTWEIGHT PHONEGAP FRAMEWORK CONFIG ////
/////////////////////////////////////////////////


$.getScript("js/piko.js");																		// load the actual piko library														



// CONFIG //
// basic variables that control the universe

$.app		 	=	"Settle";															// the slug for the app
$.version		=	0.7;
$.rdom			=	"com.company";														// the reverese-domain, excluding the app
$.firstTrans	=	"fade";																// style of first screen load. pop, fade or slide

															// the root of where we grab content
$.imSpot 		= 	"http://test.com/image-store/";										// where to find the images for the app (if off-app)
$.Pcache		=	999;																	// how many hours to cache content
$.killCache		= 	0;																	// a switch to kill the cache at every load

$.testOnWeb		=	0;																	// lets you show this via Webkit on a device, but not in Phonegap. Useful for quick tests.
$.needGeo		=	1;																	// enable geolocation fun
$.spit			=	1;																	// enable console.log outputs
$.iOSstatus		=	"#000";

$.defaultPage	=	"splash.html";

$.mapsAPI 		= "AIzaSyCi7DvRFst4hNhRUT5k7PeHQnSo60-6emw";
$.delay 		= 60000;

$.range        = 10;  //get the shop within this range in km
$.max          = 50;   //return result for the enquiry
$.display      = 3;   //row of record display at one time

$.db           = window.openDatabase("ShopInfo", "1.0", "ShopInfo", 2000000); //create database

$.resultset = {};      // the result set of the query
$.Temp = {};

$.Seq = 0;     //sequence number

$.CallBackSuccess;  // Call back function if success
$.CallBackFail;   // call back function if fail

$.index = 1;
$.number = 20;


function hashCode(s){
  var d = s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);    
  if (d < 0) {
	  d = d*-1;
  }          
  return d;
}


function setMode() {
	if ($.dist == "live") {
		$.thePort = 49237;
		$.wolfstrikeAPI = "http://services.wolfstrike.net:49237";
		$.mediaStore = "http://paycafe.wolfstrike.net/media/";
		$.fetch	= 	"http://paycafe.wolfstrike.net/api/";
		$.delay = 300000;
	} else if ($.dist == "qa") {
		$.thePort = 9237;
		$.wolfstrikeAPI = "http://servicesdev.wolfstrike.net:9237";
		$.mediaStore = "http://payqa.wolfstrike.net/media/";
		$.fetch	= 	"http://payqa.wolfstrike.net/api/";
		$.delay = 60000;
	} else if ($.dist == "dev") {
		$.thePort = 9237;
		//$.wolfstrikeAPI = "http://servicesdev.wolfstrike.net:9237";
      //  $.wolfstrikeAPI = "http://192.168.1.29:49237";
        $.wolfstrikeAPI = 'http://localhost:49237';
		$.mediaStore = "http://paycafe.wolfstrike.net/media/";
		$.fetch	= 	"http://pcdev.wolfstrike.net/api/";
		$.delay = 60000;
	}
}



// Custom vars. For custom things
// Don't look at me like that

$.cart 			= 	{};
$.cartVal 		= 	0;
$.cartCount 	= 	0;


function appInit() {

   
	if (getLS('dist') != undefined) {
		$.dist = getLS('dist');
	} else {
		$.dist = "live";
	}
	
    //*****************
	$.dist = "dev"; //set default for dev for testing 
    //****************
		
	setMode();

	//pollCache();
   
   setTimeout(function(){
			LoadShop();
		}, 2000);
    
     
   

	$('container').append('<searchSheet>');
	$.get('pages/search.html',function(d) {
		setLS('search',d);
		$('searchSheet').html(d);
	});
	
	if (getLS('myname')==undefined) {
		setLS('myname','');
	}
	if (getLS('myphone')==undefined) {
		setLS('myphone','');
	}
	
	if (window.phonegap == true) {
		if (getLS('mdid')==undefined) {
			setLS('mdid', hashCode(device.uuid));
		}
	} else {
		if (getLS('mdid')==undefined) {
			setLS('mdid', hashCode('4324132451'));
		}
	}
	
    if (getLS('userid')==undefined) {
		$.firstrun = 1;
		spit("Need to register");
		$.defaultPage = "profile.html";
	}
	/*if (getLS('myname').length<1) {
		$.firstrun = 1;
		spit("Need to fill out profile!");
		$.defaultPage = "profile.html";
	}*/
	
	document.addEventListener("pause", onPause, false);
	document.addEventListener("backbutton", onBackClickEvent, false);
	
	/*$.poller = setInterval(function() {
		pollCache();
	}, $.delay);*/
    
     $.poller = setInterval(function() {
		getInitailShop();
	}, $.delay);
    
    
    try{
    
        $.db.transaction(populateDB, errorCB, successCB);
        }
    catch(err)
    {
        alert(err);
    }
    
   

	
	spit("MDID is "+getLS('mdid'));
        
   
}

function populateDB(tx) {
     
         
         tx.executeSql('CREATE TABLE IF NOT EXISTS SHOPINFO (id unique, name, prefix, code, address, phone, coordinates, hours, holidays, categories, lastupdate, status, iconid, imagid)');       
        
    }

    // Transaction error callback
    //
    function errorCB(tx, err) {
        alert("Error processing SQL: "+err);
    }

    // Transaction success callback
    //
    function successCB(tx, results) {
        
       
    }

function InsertErrorCB(tx, err){
    
}

function InsertRow(tx, dx){
           
    alert('INSERT INTO SHOPINFO (id, name, prefix, code, address, phone, coordinates, hours, holidays, categories, lastupdate, status, iconid, imagid) VALUES ('+dx.Store.ID+','+dx.Store.Name+','+dx.Store.Prefix+','+dx.Store.Code+','+dx.Store.Address+','+dx.Store.Phone+','+JSON.stringify(dx.Store.Coordinates)+','+JSON.stringify(dx.Store.Hours)+','+JSON.stringify(dx.Store.Holidays)+','+JSON.stringify(dx.Store.Menu)+','+dx.Store.LastUpdate+','+dx.Online+','+dx.Store.LogoID+','+dx.Store.BannerID+')');
    tx.executeSql('INSERT INTO SHOPINFO (id, name, prefix, code, address, phone, coordinates, hours, holidays, categories, lastupdate, status, iconid, imagid) VALUES ('+dx.Store.ID+','+dx.Store.Name+','+dx.Store.Prefix+','+dx.Store.Code+','+dx.Store.Address+','+dx.Store.Phone+','+JSON.stringify(dx.Store.Coordinates)+','+JSON.stringify(dx.Store.Hours)+','+JSON.stringify(dx.Store.Holidays)+','+JSON.stringify(dx.Store.Menu)+','+dx.Store.LastUpdate+','+dx.Online+','+dx.Store.LogoID+','+dx.Store.BannerID+')',errorCB);
}

function UpdateRow(tx, dx){
    
    alert('UPDATE TABLE SHOPINFO SET name='+dx.Store.Name+',prefix='+dx.Store.Prefix+',code='+dx.Store.Code+',address='+dx.Store.Address+',phone='+dx.Store.Phone+',coordinates='+JSON.stringify(dx.Store.Coordinates)+',hours='+JSON.stringify(dx.Store.Hours)+',holidays='+JSON.stringify(dx.Store.Holidays)+',categories='+JSON.stringify(dx.Store.Menu)+',lastupdate='+dx.Store.LastUpdate+',status='+dx.Online+',iconid='+dx.Store.LogoID+',imageid='+dx.Store.BannerID+' WHERE id='+dx.Store.ID);
    tx.executeSql('UPDATE TABLE SHOPINFO SET name='+dx.Store.Name+',prefix='+dx.Store.Prefix+',code='+dx.Store.Code+',address='+dx.Store.Address+',phone='+dx.Store.Phone+',coordinates='+JSON.stringify(dx.Store.Coordinates)+',hours='+JSON.stringify(dx.Store.Hours)+',holidays='+JSON.stringify(dx.Store.Holidays)+',categories='+JSON.stringify(dx.Store.Menu)+',lastupdate='+dx.Store.LastUpdate+',status='+dx.Online+',iconid='+dx.Store.LogoID+',imageid='+dx.Store.BannerID+' WHERE id='+dx.Store.ID,errorCB);
}

function CheckID(tx, shopid){
   
    tx.executeSql('SELECT * FROM SHOPINFO WHERE id='+shopid,[],querySuccess, errorCB);
}

function LoadAllShop(tx, shopid){
   
    tx.executeSql('SELECT * FROM SHOPINFO',[],AllShopquerySuccess, errorCB);
}

function AllShopquerySuccess(tx, result){
     if (results.rows.length > 0)
        { 
             var dx1 = "";
             var coor ="";
             var hr = "";
             var holi = "";
             var menu = "";
             var sQuery = "";
            for(var i=0; i < $.Temp.Packet.Content.Stores.length; i++){
                                              
                for (var n=0; n<result.rows.length; n++){
                    if (result.rows.item(n).id == $.Temp.Packet.Content.Stores[i].ID){
                        
                          if ($.Temp.Packet.Content.Stores[i].Store == null){
                             $.db.transaction( function(tx){ 
                      
                             sQuery= 'UPDATE TABLE SHOPINFO SET status='+$.Temp.Packet.Content.Stores[i].Online+' WHERE id='+dx1.Store.ID;                                         
                                         
                             tx.executeSql(sQuery,[],InsertErrorCB,successCB);
                                          
                        }, errorCB);
                         }
                         else{
                        if (result.rows.item(n).lastupdate != $.Temp.Packet.Content.Stores[i].Store.LastUpdate){
                            
                        $.db.transaction( function(tx){ 
                                    
                        dx1 = $.Temp.Packet.Content.Stores[i];
                        coor = JSON.stringify(dx1.Store.Coordinates);
                        hr = JSON.stringify(dx1.Store.Hours);
                        holi = JSON.stringify(dx1.Store.Holidays);
                        menu = JSON.stringify(dx1.Store.Menu);
                       
                        sQuery= 'UPDATE TABLE SHOPINFO SET name='+dx1.Store.Name+',prefix='+dx1.Store.Prefix+',code='+dx1.Store.Code+',address='+dx1.Store.Address+',phone='+dx1.Store.Phone+',coordinates=?,hours=?,holidays=?,categories=?,lastupdate='+dx1.Store.LastUpdate+',status='+dx1.Online+',iconid='+dx1.Store.LogoID+',imageid='+dx1.Store.BannerID+' WHERE id='+dx1.Store.ID;                                         
                                         
                        tx.executeSql(sQuery,[coor,hr,holi,menu],InsertErrorCB,successCB);
                                          
                        }, errorCB);
                        }
                        }
                    }
                    
                    if (n==result.rows.length-1){
                       $.db.transaction( function(tx){ 
                                    
                        dx1 = $.Temp.Packet.Content.Stores[i];
                        coor = JSON.stringify(dx1.Store.Coordinates);
                        hr = JSON.stringify(dx1.Store.Hours);
                        holi = JSON.stringify(dx1.Store.Holidays);
                        menu = JSON.stringify(dx1.Store.Menu);
                       
                        sQuery= 'INSERT INTO SHOPINFO (id, name, prefix, code, address, phone, coordinates, hours, holidays, categories, lastupdate, status, iconid, imagid) VALUES ("'+dx1.Store.ID+'","'+dx1.Store.Name+'","'+dx1.Store.Prefix+'","'+dx1.Store.Code+'","'+dx1.Store.Address+'","'+dx1.Store.Phone+'",?,?,?,?,"'+dx1.Store.LastUpdate+'","'+dx1.Online+'","'+dx1.Store.LogoID+'","'+dx1.Store.BannerID+'")';                                          
                                         
                        tx.executeSql(sQuery,[coor,hr,holi,menu],InsertErrorCB,successCB);
                                          
                        }, errorCB);
                    } 
                    
                }
                
            }
            
            }
}


function getShopfromID(shopid){
    
     $.db.transaction( function(tx){ 
              
       
         tx.executeSql('SELECT * FROM SHOPINFO WHERE id IN ('+shopid+')',[],querySuccessResult,errorCB);
         
     }, errorCB);
                		    
}

function getShopDetailbyID(shopid){
    
     $.db.transaction( function(tx){ 
                     
         tx.executeSql('SELECT * FROM SHOPINFO WHERE id IN ('+shopid+')',[],getShopQuerySucces,errorCB);
         
     }, errorCB);
                		    
}

function ConvertDatasettoJson(dataset){
    
    var Object = {"Stores":[]};
    
    for (var i=0; i< dataset.rows.length; i++){
    var Single ={"ID": dataset.rows.item(i).id,
                 "Online": dataset.rows.item(i).status,
                 "Store": {}
    
                 }
        Single.Store = {"Address": dataset.rows.item(i).address,
                        "BannerID": dataset.rows.item(i).imageid,
                        "Code": dataset.rows.item(i).code,
                        "Coordinates": JSON.Parse(dataset.rows.item(i).coordinates),
                        "Holidays": JSON.Parse(dataset.rows.item(i).holidays),
                        "Hours": JSON.Parse(dataset.rows.item(i).hours),
                        "ID": dataset.rows.id,
                        "LastUpdate": dataset.rows.item(i).lastupdate,
                        "LogoID" :dataset.rows.item(i).iconid,
                        "Menu": JSON.Parse(dataset.rows.item(i).categories),
                        "Name": dataset.rows.item(i).name,
                        "Phone": dataset.rows.item(i).phone,
                        "Prefix": dataset.rows.item(i).prefix
            
        }
    
         Object.Stores.push(Single);
    }
    
    return Object;
    
}

function getShopQuerySucces(tx, result){
    
     if (results.rows.length > 0)
        {   
            $.resultset = result;
            SendResulttoCallBack(ConvertDatasettoJson(result));
            
             var all = JSON.parse(getLS('everything'));
            
              var shopID= shopID={'Stores':[],
                        'Type':'GetStoreReqContent',
                         'Ver':'1.00'    } ;
        
              var single = {};
            
            for (var j=0; j< $.display; j++){
                
            for (var i=0; i<result.rows.length; i++){
                  if (result.rows.item(i).id == all.Packet.Content.Stores[j].ID){
                     
                     if (results.rows.item(i).lastupdate != all.Packet.Content.Stores[j].LastUpdate)
                       {
                          single = {'ID':all.Packet.Content.Stores[j].ID,
                          'LastUpdate':all.Packet.Content.Stores[j].LastUpdate};                                               
                       }                       
                      break;                      
                  }
                
                  if (i == result.rows.length-1){
                      
                          single = {'ID':all.Packet.Content.Stores[j].ID,
                          'LastUpdate':all.Packet.Content.Stores[j].LastUpdate}; 
                      }
                 shopID.Stores.push(single);
             }
            
        }
            
            getShopDetail(shopID);
    
    }
    
}

function SendResultSettoCallBAck(result){
    
    var all = getLS('everything');
    
    for (var i=0; i < $.display; i++){
        for (var n=0; n < result.Stores.length; n++){
            if (all.Packet.Content.Stores[i].ID == result.Stores[j].ID){
                if (i+1 >= $.index){ 
                    $.CallBackSuccess(i+1,result.Stores[j]);
                    }
                break;
            }
        }
    }
    
}



function getShopfromIDResultset(shopid){
             
    var shop = $.resultset.rows.item(0);
    try{ 
        for (var i=0; i<$.resultset.rows.length; i++){
            if (shopid == $.resultset.rows.item(i).id){
                shop = $.resultset.rows.item(i);
                break;
            }
        }
        
        }
         catch(err){
               alert(err);
             }
           
    return shop;
}



function querySuccessResult(tx, results){
    
    try{
             
        if (results.rows.length > 0)
        {
              $.resultset = results
            }
        
        }
    catch(err){
        alert(err);
    }        
}


function CheckShopOpen(dx)
{
    var bOpen = true;
    
    var today = new Date();
    var date = today.getDate()+'/'+today.getMonth()+'/'+today.getFullYear();
    var day = today.getDay();
    var holi = JSON.parse(dx.holidays);
    var hr = JSON.parse(dx.hours);
    
    var newstart = "";
    var newend = "";    
       
    for (var i=0; i < holi.length; i++)
    {
        if (date == holi[i].Day)
        {
            bOpen = false;
            if (holi[i].StartTime != "-1"){
                   
                newstart = holi[i].StartTime;
                newend = holi[i].EndTime;                
            }
            break;
        }
    }
    
    if (bOpen == true){
        
      
        if (hr[day-1].StartTime == "0" && hr[day-1].EndTime == "0")
        {
            bOpen = true;
        }
        else if (hr[day-1].StartTime == "-1")
        {
            bOpen=false;
            }
        else{
            bOpen = CheckTime(hr[day-1].StartTime, hr[day-1].EndTime);
            }
    }
    else
    {
        if (newstart != ""){
             bOpen = CheckTime(newstart, newend);
        }
    }
    
   /* if (bOpen == true)
    {
        if (day == 1)
        {
            if (hr.mon == undefined)
            {
                bOpen = false;
            }
            else{
                
                bOpen = CheckTime(hr.mon);                               
            }
        }
        else if (day == 2){
            
             if (hr.tue == undefined)
            {
                bOpen = false;
            }
            else{
                
                bOpen = CheckTime(dx.hours.tue);                               
            }
        }
        else if (day == 3){
            
             if (dx.hours.wed == undefined)
            {
                bOpen = false;
            }
            else{
                
                bOpen = CheckTime(dx.hours.wed);                               
            }
        }
          else if (day == 4){
            
             if (dx.hours.thu == undefined)
            {
                bOpen = false;
            }
            else{
                
                bOpen = CheckTime(dx.hours.thu);                               
            }
        }
          else if (day == 5){
            
             if (dx.hours.fri == undefined)
            {
                bOpen = false;
            }
            else{
                
                bOpen = CheckTime(dx.hours.fri);                               
            }
        }        
          else if (day == 6){
            
             if (dx.hours.sat == undefined)
            {
                bOpen = false;
            }
            else{
                
                bOpen = CheckTime(dx.hours.sat);                               
            }
        }
          else if (day == 7){
            
             if (dx.hours.sun == undefined)
            {
                bOpen = false;
            }
            else{
                
                bOpen = CheckTime(dx.hours.sun);                               
            }
        }
        
    }*/
    return bOpen;
}

function CheckTime(start,end){
   /*  var hr = start.substring(0,2);
     var min = start.substring(2,4);
     var starttime = new Date();
     starttime.setHours(hr,min,0);
     hr = end.substring(0,2);
     min = end.substring(2,4);
     var endtime = new Date();
     endtime.setHours(hr,min,0);*/
    
     var hour = new Date().getHours();
     var mins = new Date().getMinutes();
     var h = hour.toString();
     var m = mins.toString()
     var time = h+m;
     var now = parseInt(time);
    
    if ((now > start) && (now < end))
    {
        return true;
    }
    else
    {
        return false;
    }
     
}

function getTime(hour){
    var today = new Date();
    var num = today.getDay();
    var text = "";
    if (hour[num-1].StartTime == '-1'){
        text = "Closed Today";
    }
    else if (hour[num-1].StartTime == '0'){
        text = "Open 24 hours";
    }
    else{
        text = "Open From "+hour[num-1].StartTime+" To "+hour[num-1].EndTime;
    }
    
    return text;
}



function querySuccess(tx, results){
    
    if (results.rows.length == 0)
    {
        $.bhasid = false;
    }else{
        
        $.bhasid = true;
        $.dblastupdate = results.rows.item(0).lastupdate;
    }
}



function getInitialSuccess(dx)
{
    
    setLS('everything',JSON.stringify(dx));
    setLS('lastCache',new Date().getDate());
    setLS('lastLat',$.myLat);
    setLS('lastLon',$.myLon);
}

function getInitailShop(){
    
    
    checkLocation($.needGeo);
    
    var dis = distances(getLS('lastLat'),getLS('lastLon')).toFixed(1);
    var date = new Date().getDate();
    var datespan = date - getLS('lastCache');
    
    if (dis > ($.range/2)){
        LoadShop();
        }
    else if (datespan > 1)
    {
        LoadShop();
    }   
      
}

function InitiliseSpalsh(index, number, CallBackSuccess,CallBackFail){
   
    $.CallBackSuccess = CallBackSuccess;
    $.CallBackFail = CallBackFail;
    $.index = index;
    $.number = number;
    $.display = index + number - 1;
    
                    
    if ($.diaplay > $.max)
    {
        $.display = $.max;
    }
                  
    var all = JSON.parse(getLS('everything'));
                   
    if (all.Packet.Content.Stores.length < $.display){
        $.display = all.Packet.Content.Stores.length;
    }
        
      
    var shopID={'Stores':[],
                'Type':'GetStoreContent',
                'Ver':'1.00'                       
        };
        
    var single = {};
        
    if (getLS('database') == undefined){
           
       
        for (var i=0;i<$.display;i++){
            
            single = {'ID':all.Packet.Content.Stores[i].ID,
                     'LastUpdate':'-1'
            }
            
           shopID.Stores.push(single);
        }        
                
        getShopDetail(shopID);
            
       
    }
    else
    {
     
        getShop();
       
       // var database = JSON.parse(getLS('database'));
                                                       
           /* for (var n=0; n<database.Shops.length; n++){
                if (database.Shops[n].ID == all.Shops[j].ID)
                {
                    if (database.Shops[n].LastUpdate != all.Shops[j].LastUpdate)
                    {
                         shopID+='{"StoreID":'+all.Shops[j].ID+',';
                         shopID += '"LastUpdate":'+all.Shops[j].LastUpdate+'}';
                    }
                    break;
                }
                
                if (n == database.Shops.length-1)
                {
                      shopID+='{"StoreID":'+all.Shops[j].ID+',';
                      shopID += '"LastUpdate":'+all.Shops[j].LastUpdate+'}';
                }
            }*/
                       
        
        
       
        
    }
   
   
    
    
}

function getShop(){
    
    var all = getLS('everything');
     var AllID = "";
            
        for (var n=0; n < $.display; n++)
        {
            if (n == 0){
            AllID += "'"+all.Packet.Content.Stores[n].ID+"'";
            }
            else{
            AllID += ",'"+all.Packet.Content.Stores[n].ID+"'";
            }
        }
        getShopDetailbyID(AllID);
}


function getShopDetailSuccess(dx)
{
   
    
    try{
     if (getLS('database') == undefined){
                 
        if (dx.Packet.Content.Stores.length>0){
            setLS('database','Initialise');
        }
         
         var sQuery ="";
                   
           for (var i=0; i<dx.Packet.Content.Stores.length;i++)
            {
               $.db.transaction( function(tx){ 
                                    
                       var dx1 = dx.Packet.Content.Stores[i];
                       var coor = JSON.stringify(dx1.Store.Coordinates);
                       var hr = JSON.stringify(dx1.Store.Hours);
                       var holi = JSON.stringify(dx1.Store.Holidays);
                       var menu = JSON.stringify(dx1.Store.Menu);
                       
                        sQuery= 'INSERT INTO SHOPINFO (id, name, prefix, code, address, phone, coordinates, hours, holidays, categories, lastupdate, status, iconid, imagid) VALUES ("'+dx1.Store.ID+'","'+dx1.Store.Name+'","'+dx1.Store.Prefix+'","'+dx1.Store.Code+'","'+dx1.Store.Address+'","'+dx1.Store.Phone+'",?,?,?,?,"'+dx1.Store.LastUpdate+'","'+dx1.Online+'","'+dx1.Store.LogoID+'","'+dx1.Store.BannerID+'")';                                          
                                         
                        tx.executeSql(sQuery,[coor,hr,holi,menu],InsertErrorCB,successCB);
                       
                   
               }, errorCB);
                       
            }
         
                SendResultSettoCallBAck(dx.Packet.Content);
         
         }
    else{
       
        
        
      //  var obj = JSON.parse(getLS('database'));
       
           SendResultSettoCallBAck(dx.Packet.Content);
            $.Temp = dx;
             $.db.transaction( function(tx){ LoadAllShop(tx, dx.Packet.Content.Stores[i].Store.ID) }, errorCB);  
                                                     
          /*  for (var n=0; n<obj.Shops.length; n++){
                if (dx.Shops[i].ID == obj.Shops[n].ID){
                    obj.Shops[n] = dx.Shops[i]; //update
                    
                    break;
                }
                
                if (n == dx.Shops.length-1)
                {
                    obj.Shops.push(dx.Shop[i]);  //insert
                }
                
            }*/
            
           
        
        
    }
        }
    catch(err)
    {
        alert(err);
    }
}

function getShopDetail(shopID){
    var content = shopID;
        
    SendJSonMsg('GetStore', content, getShopDetailSuccess);
}
    
function LoadShop(){
    
                                  
     var content = {'Query':'',
                   'Lat': $.myLat,
                   'Long':$.myLon,
                   'Range':$.range,
                   'Max':$.max,
                   'SortBy':'Distance',
                   'Type':'FindStoreContent'          
    }
    
    SendJSonMsg('FindStore', content, getInitialSuccess);
                             
    }



function onBackClickEvent() {
    loadPage($.goBack,"slideR");   
}


function onPause() {
	navigator.device.exitApp();
}


function pollCache() {
	spit("Polling cache now... "+$.fetch+"?dt="+getLS('lastCache'));
	$.getJSON($.fetch+"?dt="+getLS('lastCache'), function(cx) {																// get the data we're after via JSON
		if (cx == "OK") {
			spit("No changes since last time");
		} else {
			spit("Got new data");
			setLS('everything',JSON.stringify(cx));															// store it in LS
			setLS('everything.time',new Date().getTime());													// and set the cache time to now
			setLS('lastCache',cx['last']);
		}
	})
}




function firstPrep() {
	var thecart = getLS('cart');
	if (thecart == undefined) {
		var thecart = '{}';
	}
	$.cart = JSON.parse(thecart);
}


function lastPrep() {
	parseCart();
	$.searches = JSON.parse(getLS('everything')).search;
	$('.header').addClass($.dist);
}

function SendJSonMsg(cmd, content, SuccessFunction){
            var out = {};
			var date = new Date();
            var seq = getLS('seq');
			out['UDID']		=	getLS('mdid');
			out['Token']	= 	getLS('userid');
			out['Seq']		=	seq;
			out['Time']		=	date.toISOString().replace('Z','');
			out['App']		=	'TermMsg';
			out['Cmd']		=	cmd;
			out['Content']	=	content;
   
			//out['Mac']		=	'';
			out['Type']		=	'BasicIncPkt';    
            //out['Type']		=	'StdPkt';
    
			out['Ver']		=	1.0;
    
    $.ajax({
			    url: $.wolfstrikeAPI,
			    type: 'post',
			    data: JSON.stringify({'Packet': out }),
			    headers: {
					'Accept': 'text/json',
					'User-Agent': 'Fiddler',
					'Host': 'wsservices.cloudapp.net:'+$.thePort
			    },
			    dataType: 'json',
			    success: function (data) {
                                        
                   SuccessFunction(data);
			    },
			    error: function(xhr, errorType, exception) {
			    	
			    	var errorMessage = exception || xhr.statusText;
				    showError("Error", "There was an error sending your order. Please try again. ("+errorMessage+")", "OK", "sendReceipt");
			    }
			});
    
}   



$(document).ready(function() {
	$('body').on("click", ".docart", function() {
		var x = $(this);
		if ($.cart[getLS('biz')][$.uc] && $.cart[getLS('biz')][$.uc]['addThis'] > 0) {
/*
			$.cart[getLS('biz')][$.uc]['addThis'] = 0;
			setLS('cart',JSON.stringify($.cart));
			$(this).html('Add to cart');
*/
			parseCart();
		} else {
			doCart(x.attr('cartCode'), x.attr('cartName'), x.attr('cartVal'),x.attr('cartType'),-1,$('.numItems').val());
			setLS('uc',$.uc);
		}
	})
	.on('click', '#numItems', function() {
		$(this).val('');
	})
	.on("change", "#numItems", function() {
		var x = $(this);
		doCart(x.attr('cartCode'), x.attr('cartName'), x.attr('cartVal'),'top');	
	})
	.on("click", ".summary .add_item", function(e) {
/*
		e.stopPropagation();
		e.preventDefault();	
*/
		var iam = $(this).parent().parent().attr('uc');
		$.cart[getLS('biz')][iam]['addThis'] = ($.cart[getLS('biz')][iam]['addThis']*1) +1;
		setLS('cart', JSON.stringify($.cart));
		$(this).parent().parent().find('.numofit').html($.cart[getLS('biz')][iam]['addThis']);
		parseCart();
		
/*
		var newcart = new Date().getTime();
		if ($.cart[getLS('biz')] == undefined) {
			$.cart[getLS('biz')] = {};
		}
		$.cart[getLS('biz')][newcart] = cl;
		setLS('cart',JSON.stringify($.cart));
		var me = $(this).parent().parent().clone();
		var cont = $(this).parent().parent().parent();
		$(me).hide().appendTo(cont).slideDown();
*/
	})
	.on("click", ".summary .remove_item", function(e) {
		e.stopPropagation();
		e.preventDefault();
		var iam = $(this).parent().parent().attr('uc');
		$.cart[getLS('biz')][iam]['addThis'] = ($.cart[getLS('biz')][iam]['addThis']*1) -1;
		setLS('cart', JSON.stringify($.cart));
		if ($.cart[getLS('biz')][iam]['addThis'] == 0) {
			$(this).parent().parent().slideUp(500, function() {
				var cl = $('.itemlist:visible').length;
				if (cl == 0) {
					$('a.summary').fadeOut();
				}
			});
		} else {
			$(this).parent().parent().find('.numofit').html($.cart[getLS('biz')][iam]['addThis']);
		}
		parseCart();
/*
		$.cart[getLS('biz')][iam]['addThis'] = 0;
		setLS('cart',JSON.stringify($.cart));
		$(this).parent().parent().slideUp(function() {
			loadPage("summary.html","pop");
		});
*/
	})
	.on("click", ".page.item .itemlist", function(e) {
		e.stopPropagation();
		var x = $(this);
		doCart(x.attr('cartCode'), x.attr('cartName'), x.attr('cartVal'),x.attr('cartType'));
	})
	.on("click", ".page.item .itemlist", function() {
		var x = $(this);
		doCart(x.attr('cartCode'), x.attr('cartName'), x.attr('cartVal'),x.attr('cartType'),-1);
	})
	.on("click", ".subitemlist", function() {
		$(this).parent().find('.toggle_item').removeClass('picked');
		$(this).find('.toggle_item').addClass('picked');
		var x = $(this);
		var worth = parseFloat(x.attr('cartVal'));
		$.each(x.parent().find('.subitemlist'),function() {
			var iam = parseFloat($(this).attr('cartVal'));
			var dos = iam-worth;
			if (dos < 0) {
				dos = "- $"+(dos*-1).toFixed(2);
			} else {
				dos = "+ $"+dos.toFixed(2);
			}
			$(this).find('.toggle_item').html(dos);
		});
		doCart(x.attr('cartCode'), x.attr('cartName'), x.attr('cartVal'),x.attr('cartType'), 'op');
	})
	.on("click", ".searchme, .search", function() {
		$('searchSheet').css({'top':$('container').height()*-1, 'width':$(window).width(), 'height':$('container').height()}).show().html(getLS('search')).animate({'top':0});
	})
	.on("click", "searchSheet .allClose", function() {
		$('searchSheet').animate({'top':$('container').height()*-1}, function() {
			$(this).html('').hide();
		});
	})
	.on('change', '#pickuptimer', function() {
		var i = $(this).val();
		$('#pickup').val(i);	
	})
	.on('change', '#pickup', function() {
		var i = $(this).val();
		$('#pickuptimer').val(i);	
	})
	.on('submit', '#searchingForm', function(d) {
		$('input').blur();
		return false;	
	})
	.on("keyup", ".realSearch", function() {
		var getme = $(this).val();
		var x = {};
		$('.searchresults').html('');
		var gotcha = '';
		$.each($.searches, function(key,value) {
			if (value.s.toLowerCase().search(getme.toLowerCase())>-1) {
				gotcha += '<a href="biz.html" biz="'+key+'" move="pop">'+value.n+'</a>';
			}
		});
		$('.searchresults').html(gotcha);
	})
	.on("click", ".searchresults a", function() {
		$('searchSheet').animate({'top':$('container').height()*-1}, function() {
			$(this).html('').hide();
		});
	})
	.on("keydown", ".sideInfo input", function() {
		var d = $(this).attr('id');
		setLS('my'+d, $(this).val());
	})
    
    .on("click", ".PasswordOK", function() {
        
         setLS('myphone', $('#loginphone').val());
         setLS('myemail', $('#loginemail').val());
		 setLS('mypassword', $('#loginpassword').val());
        CheckPassword();
    })
    
    .on("click", ".resend", function() {
        
         var num = Math.floor(Math.random() * 10000); //random 4 digit number
                    setLS('random',num);
                     window.setTimeout( function(){ 
                            setLS('random','-1');
                            }, 600000 ); //code expire in 10 mins
                    
                  
                   // window.location.href ='https://api.clickatell.com/httndmsg?user=rogerwang&password=wolfstrike123&api_id=3459266&to='+$('#phone').val()+'&text='+num;
                    window.open('https://api.clickatell.com/http/sendmsg?user=rogerwang&password=wolfstrike123&api_id=3459266&to='+$('#phone').val()+'&text='+num,'_self',false);        
        })
    
    .on("click", ".repassword", function() {
        
        if ($('#loginemail').val()!="")
        {
           // ResetPassword();
            showError("Sent", "your password will be sent to your email", "OK");
        }
        else{
            
            showError("Oops!", "Please enter your email address, the password will be sent to your email", "OK");
        }
    })
    
	.on("click", ".allOK", function() {
        
                
		if ($.firstrun == 1) {
			if ($('#name').val()!="" && $('#phone').val()!="" && $('#password').val()!="" && $('#Con_password').val()!=""){
                
                if ($('#password').val()!=$('#Con_password').val())
                {
                    showError("Oops!", "Password and confirm password must be the same, please try again.", "OK");
                }                
                else
                {
                   
                   /* var num = Math.floor(Math.random() * 10000); //random 4 digit number
                    setLS('random',num);
                     window.setTimeout( function(){ 
                            setLS('random','-1');
                            }, 600000 ); //code expire in 10 mins*/
                                      
                  
                   // window.open('https://api.clickatell.com/http/sendmsg?user=rogerwang&password=wolfstrike123&api_id=3459266&to=64211057100&text='+num,'_self',false);
                  // SendSMSMsg(num);
                    if (getLS('seq')==undefined) {
				        setLS('seq',500);
			           }
                    
                    var content = {
                   'FirstName':$('#firstname').val(), 
                   'LastName':$('#lastname').val(),  
                   'Email':$('#email').val(),
                   'Password': $('#password').val(),                                                                                 
                   'PhoneNum': $('#phone').val(),    
                   'Type':'LoadUserContent'          
                    }
    
                     SendJSonMsg('LoadUser', content, RequestSuccess);
                  
                    
                    $.firstrun = 2;
                   // alert(num);
                    setLS('myname', $('#firstname').val()+' '+$('#lastname').val());
                    setLS('myfirstname',$('#firstname').val());
                    setLS('mylastname',$('#lastname').val());
		            setLS('myphone', $('#phone').val());
                    setLS('myemail', $('#email').val());
		            setLS('mypassword', $('#password').val());
                    loadPage("verification.html", "slide");
				   // showError("Thank you!", "Your details have been saved.", "OK", "goHome");
				   //  $.firstrun = 0;
                }
			} else {
				showError("Oops!", "Please fill in all fields to get started.", "OK");
			}
		} else {
            
            if ($.firstrun == 2)
            {
                /*if (getLS('random') == "-1")
                {
                    showError("Oops!", "The verification code is expired, please register again.", "OK");
                    loadPage("profile.html", "slide");
                    
                }
                else{
                    
                     if ($('#code').val() == getLS('random'))
                     {
                         getUserID();
                        
                     }
                     else
                     {
                           showError("Oops!", "The verification code is incorrect.", "OK");
                     }
                    }*/
                 var content2 = {
                                     
                      'Email':getLS('myemail'),                   
                      'Verification': $('#code').val(),  
                      'Type':'VerifyContent'          
                    }
    
                    SendJSonMsg('Verify', content2, VerificationSuccess);
            }
            else
            {
                goHome();
            }
           
		}
		
		
	})
	.on("click", ".lastorder", function() {
		$.cart = JSON.parse(getLS('cart'));
		var lastorder = JSON.parse(getLS('oldCart'));
		lastorder = lastorder[$(this).attr('lastorder')];
		$.cart[lastorder['biz']] = lastorder['items'];	
		setLS('cart',JSON.stringify($.cart));
		window.history.pushState(null, null, 'index.html?page=summary.html');
		loadPage("summary.html", "slide");

	})
	.on('click', '.clearorder', function() {
		$.cart[getLS('biz')] = {};
		setLS('cart', JSON.stringify($.cart));
		loadPage('summary.html','pop');	
	})
	.on('change', '.page.send input', function() {
		spit('ok');
		if ($('#name').val() != '' && $('#phone').val() != '' && $('#pickup').val() != '') {
			$('.sendnow').removeClass('off');
		} else {
			$('.sendnow').addClass('off');
		}
	})
	.on("click", ".sendnow", function() {
		$('.spinnerContainer').fadeIn(100);															// fade in the spinner (loading thing)
		sendReceipt();	
	})
    
    .on("click", ".paynow", function() {
		$('.spinnerContainer').fadeIn(100);															// fade in the spinner (loading thing)
		loadPage("gateway.html", "slide");	
	})
    
     .on("click", ".loadmore", function() {
		 
         try{
               var list = JSON.parse(getLS('everything'));
                          
         if (($.display+20) > list.Packet.Content.Stores.length){
             $.display = list.Packet.Content.Stores.length;
             
         }
         else
         {
             $.display += 20;
         }
                   
             LoadShop();
         
          alert('loadpage');   
          loadPage("splash.html", "slide");
          
         }
         catch(err)
        {
           alert(err);
        }
         
	})
	
	.on("touchend mouseup", function(e) {
		if ($.konamiTimer == undefined) {
			$.konami = ['up','up','down','down','left','right','left','right'];
			$.konamiTimer = setTimeout(function() {
				clearTimeout($.konamiTimer);
				delete($.konamiTimer);
			},10000);
		}
		var wh = $(window).height();
		var ww = $(window).width();
		var mc = pointerEventToXY(e);
		var ptop = 100/wh * mc.y;
		var plft = 100/ww * mc.x;
		var need = $.konami[0];
		if (need == "up" && ptop <= 25) {
			$.konami.shift();
		} else if (need == "down" && ptop >= 75) {
			$.konami.shift();
		} else if (need == "left" && plft <= 25) {
			$.konami.shift();
		} else if (need == "right" && plft >= 75) {
			$.konami.shift();
		} else {
			delete($.konami);
			clearTimeout($.konamiTimer);
			delete($.konamiTimer);
		}
		if ($.konami && $.konami.length == 0) {
			if ($.dist == "live") {
				$.dist = "qa";
			} else if ($.dist == "qa") {
				$.dist = "dev";
			} else if ($.dist == "dev") {
				$.dist = "live";
			}
			$('.header').removeClass('qa').removeClass('dev').addClass($.dist);
			setMode();
			setLS('dist',$.dist);
			setLS('lastCache',0);
			pollCache();
			showError("Debug Mode", "Mode switched to "+$.dist.toUpperCase(), "OK", "goHome");
		}
		
	});
			

 var pointerEventToXY = function(e){
      var out = {x:0, y:0};
      if(e.type == 'touchstart' || e.type == 'touchmove' || e.type == 'touchend' || e.type == 'touchcancel'){
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        out.x = touch.pageX;
        out.y = touch.pageY;
      } else if (e.type == 'mousedown' || e.type == 'mouseup' || e.type == 'mousemove' || e.type == 'mouseover'|| e.type=='mouseout' || e.type=='mouseenter' || e.type=='mouseleave') {
        out.x = e.pageX;
        out.y = e.pageY;
      }
      return out;
    };
    
 function SendSMSMsg(num){
     
    try{
      $.ajax({
			    url: 'https://api.clickatell.com/http/sendmsg?user=rogerwang&password=wolfstrike123&api_id=3459266&to='+$('#phone').val()+'&text='+num,
			    type: 'post',
			  
			    success: function (data) {
                    
                  
			    },
			    error: function(xhr, errorType, exception) {
			    	
			    	var errorMessage = exception || xhr.statusText;
				    showError("Error", "There was an error sending SMS. Please try again. ("+errorMessage+")", "OK", "sendSMS");
			    }
			});
        }
     catch(err)
     {
         alert(err);
     }
 }  
    
 function VerificationSuccess(dx){
     
     var seq = getLS('seq');
      var obj = JSON.parse(data);
                        
                                        
                    if (obj.Packet.Seq == seq){
                        
                        if (obj.Packet.Cmd == 'CodeSuccessed')
                        {					        
                            setLS('seq',parseFloat(seq)+1);
                            loadPage("splash.html", "slide"); 
                        }
                        else if (obj.Packet.Cmd == 'CodeFailed')
                        {
                           showError("Oops!", "The verification code is not correct. Please check again or click on 'Send again' if not received", "OK");
                        }                     
                        else
                        {
                            showError("Oops!", "Error! please try again.", "OK");
                        }
                    }
                    else
                    {
                        showError("Oops!", "Error! please try again.", "OK");
                    }
     
 }   
    
 function RequestSuccess(dx){
     
     var seq = getLS('seq');
      var obj = JSON.parse(data);
                        
                                       
                    if (obj.Packet.Seq == seq){
                        
                        if (obj.Packet.Result == 'OK')
                        {					        
                            setLS('seq',parseFloat(seq)+1);
                            loadPage("verification.html", "slide"); 
                            
                        }
                        else if (obj.Packet.Result == 'USER_EXISTS')
                        {
                           showError("Oops!", "The email is already registered. Please use another one or Click 'Already have account'.", "OK");
                        }
                        else if ((obj.Packet.Result == 'TOKEN_FAILED') || (obj.Packet.Result == 'TOKEN_EXPIRED'))
                        {
                            showError("Oops!", "The login session has expired, Please login again", "OK");
                            loadPage("login.html", "slide");
                        }
                        else
                        {
                            showError("Oops!", "Error! please try again.", "OK");
                        }
                    }
                    else
                    {
                        showError("Oops!", "Error! please try again.", "OK");
                    }
     
 }
    
 function CheckPassword(){
           var out = {};
			var date = new Date();
    
     
     var seq = getLS('seq');
			out['Term']		=	getLS('mdid');
			out['Merch']	= 	'000';
			out['Seq']		=	seq;
			out['Time']		=	date.toISOString().replace('Z','');
			out['App']		=	'TermMsg';
			out['Cmd']		=	'CheckPassword';
			out['Content']	=	{								
								'Email':getLS('myemail'),
								'Password':getLS('mypassword'),
								'PhoneNum':getLS('myphone'),
                                'Type':'CheckPwdContent'
								},
			out['Mac']		=	'';
			out['Type']		=	'StdPkt';
			out['Ver']		=	1.0;
    
    $.ajax({
			    url: $.wolfstrikeAPI,
			    type: 'post',
			    data: JSON.stringify({'Packet': out }),
			    headers: {
					'Accept': 'text/json',
					'User-Agent': 'Fiddler',
					'Host': 'wsservices.cloudapp.net:'+$.thePort
			    },
			    dataType: 'json',
			    success: function (data) {
                    
                     var obj = JSON.parse(data);
                    
                    if (obj.Seq == seq){
                        
                        if (obj.Cmd == 'PwdOK'){
                             setLS('userid',obj.Content);
                             setLS('seq',parseFloat(seq)+1);
                             goHome();
                        }
                        
                        else if (obj.Cmd == 'Verification')
                        {
                             var num = Math.floor(Math.random() * 10000); //random 4 digit number
                             setLS('random',num);
                             window.setTimeout( function(){ 
                            setLS('random','-1');
                            }, 600000 ); //code expire in 10 mins                    
                                      
                             window.open('https://api.clickatell.com/http/sendmsg?user=rogerwang&password=wolfstrike123&api_id=3459266&to=64211057100&text='+num,'_self',false)
                             $.firstrun = 2;
                             loadPage("verification.html", "slide");
                        }
                        else{
					           showError("Oops!", "Incorrect Password! please try again.", "OK");
                            }
                        }
                    else
                    {
                        showError("Oops!", "Error! please try again.", "OK");
                    }
			    },
			    error: function(xhr, errorType, exception) {
			    	
			    	var errorMessage = exception || xhr.statusText;
				    showError("Error", "There was an error sending your order. Please try again. ("+errorMessage+")", "OK", "sendReceipt");
			    }
			});
        }
    
    
/*function ResetPasswordSuccess(data){
    
}    
function ResetPassword(){
    var content = {"email":$('#loginemail').val()};
    SendJSonMsg("Reset Password", content, ResetPasswordSuccess);
}  */  
function getUserID(){
        
    
    var out = {};
			var date = new Date();
   
    
    
    var seq = getLS('seq');
			out['Term']		=	getLS('mdid');
			out['Merch']	= 	'000';
			out['Seq']		=	seq;
			out['Time']		=	date.toISOString().replace('Z','');
			out['App']		=	'TermMsg';
			out['Cmd']		=	'LoadUser';
			out['Content']	=	{
                                'FirstName':getLS('myfirstname'),
								'LastName':getLS('mylastname'),
								'Email':getLS('myemail'),
								'Password':getLS('mypassword'),
								'PhoneNum':getLS('myphone'),
                                'Type':'LoadUserContent'
								},
			out['Mac']		=	'';
			out['Type']		=	'StdPkt';
			out['Ver']		=	1.0;
   
    
    $.ajax({
			    url: $.wolfstrikeAPI,
			    type: 'post',
			    data: JSON.stringify({'Packet': out }),
			    headers: {
					'Accept': 'text/json',
					'User-Agent': 'Fiddler',
					'Host': 'wsservices.cloudapp.net:'+$.thePort
			    },
			    dataType: 'json',
			    success: function (data) {
                    
                    alert('success');
                    
                    try{
                        
                    var obj = JSON.parse(data);
                        alert(obj.Cmd);
                                        
                    if (obj.seq == seq){
                        
                        if (obj.Cmd == 'UserLoaded')
                        {
					        setLS('userid',obj.Content.UserID);
                            setLS('seq',parseFloat(seq)+1);
                            goHome();   
                        }
                        else if (obj.Cmd == 'UserExists')
                        {
                           showError("Oops!", "The email is already registered. Please use another one or Click 'Already have account'.", "OK");
                        }
                        else if (obj.Cmd == 'UserNumRegistered')
                        {
                            showError("Oops!", "The phone number is already registered, please use another one or Click 'Already have account'.", "OK");
                        }
                        else
                        {
                            showError("Oops!", "Error! please try again.", "OK");
                        }
                    }
                    else
                    {
                        showError("Oops!", "Error! please try again.", "OK");
                    }
                        }
                    catch(err)
                    {
                        alert(err);
                    }
			    },
			    error: function(xhr, errorType, exception) {
			    	
			    	var errorMessage = exception || xhr.statusText;
				    showError("Error", "There was an error sending your order. Please try again. ("+errorMessage+")", "OK", "sendReceipt");
			    }
			});
    
    }
    

    
function sendReceipt(i) {

	spit("trying to send");
			
	if ($.sendNow != 1) {
	
	$.sendNow = 1;
	setLS('myname',$('#name').val());
	setLS('myphone',$('#phone').val());
	
	if ($(this).hasClass('off')) {
		
	} else {
		var od = JSON.parse(getLS('cart'));
		var mod = Object.keys(od[getLS('biz')])[0];
		
		if (mod != undefined) {
			var pickup = moment($('#pickup').val());
		
			if (getLS('seq')==undefined) {
				setLS('seq',500);
			}
			var seq = getLS('seq');
			
			var dx = JSON.parse(getLS('everything'));
			var iam = dx.shops[getLS('biz')];
			
			var out = {};
			var date = new Date();
			out['Term']		=	getLS('mdid');
			out['Merch']	= 	iam.prefix + "-" + iam.code;
			out['Seq']		=	seq;
			out['Time']		=	date.toISOString().replace('Z','');
			out['App']		=	'TermMsg';
			out['Cmd']		=	'LoadMessage';
			out['Content']	=	{
								"Merch":iam.prefix + "-" + iam.code,
								'Logo':'000017',
								'Type':'LoadMsgContent',
								'Ver':1.0
								},
			out['Mac']		=	'';
			out['Type']		=	'StdPkt';
			out['Ver']		=	1.0;
	
	
			setLS('seq',parseFloat(seq)+1);
					
			var dt = moment(date).format("hh:mma");
			var rp = '';
			$.each(od[getLS('biz')], function(iid, id) {
				if (id.addThis > 0) {
					rpp = '';
					
					if (id.options.Size) {
						id.val = parseFloat(id.val) + parseFloat(id.options.Size.val);
						id.name = id.options.Size.name + ' ' + id.name;
						id.options.Size = "";
					}
					
					rp += makeLine(id.addThis + " x " + id.name, "$" + (id['val']*1).toFixed(2), 4);
		
					$.each(id.options, function(oip, ip) {
						if (oip != "" && ip.name != undefined && ip.name != "undefined" && ip.name!="") {
							if (ip.val*1 > 0) {
								rp += makeLine("  + " + ip.name + " ", "$" + ip.val, 5);
							} else {
								rp += makeLine("  + " + ip.name + " ", "", 5);
							}
						}
					});
					rp += makeLine("");
				}
	
	
			});
			
			
			var messa = iam.name.toUpperCase();
			var divi = (23-messa.length)/-2
			messa = String("       ").slice(divi) + messa + String("       ").slice(divi);
			var tt = $('.price').html().split(" ")[1];
	
	var d	 = "";			
/*
	var d 	 = makeLine("----------------------");
	d 		+= makeLine("");
	d 		+= makeLine(" " + messa + " ");
	d 		+= makeLine("");
	d 		+= makeLine("Order:  "+mod);
	d 		+= makeLine("Time:   "+dt);
	d 		+= makeLine("Pickup: " + moment(pickup).format("hh:mma DD/MM/YYYY"));
	d 		+= makeLine("Branch: "+iam.address, '', 8);
	d 		+= makeLine("");
	d 		+= makeLine("----------------------");
*/
	d 		+= rp;
/* 	d 		+= makeLine(""); */
	d 		+= makeLine("-------------------------");
/* 	d 		+= makeLine(""); */
	d 		+= makeLine("Amount Due:", "$" + tt);
	d 		+= makeLine("");
	d 		+= makeLine("Time: " + moment(pickup).format("hh:mma DD/MM"));
	d 		+= makeLine("Name: "+$('#name').val());
	d 		+= makeLine("	 (" + $('#phone').val() + ")");
/*
	d 		+= makeLine("");
	d 		+= makeLine("----------------------");
*/
			spit(d);
			out['Content']['Text'] = d;
			spit(out);
			$.ajax({
			    url: $.wolfstrikeAPI,
			    type: 'post',
			    data: JSON.stringify({'Packet': out }),
			    headers: {
					'Accept': 'text/json',
					'User-Agent': 'Fiddler',
					'Host': 'wsservices.cloudapp.net:'+$.thePort
			    },
			    dataType: 'json',
			    success: function (data) {
					$.post('http://paycafe.wolfstrike.net/api/', {'term':getLS('mdid'), 'merch':getLS('biz'), 'seq':seq, 'content':d});
					
					if (getLS('oldCart')) {
						$.oldCart = JSON.parse(getLS('oldCart'));
					} else {
						$.oldCart = {};
					}
					$.oldCart[date.toTimeString()] = {};
					$.oldCart[date.toTimeString()]['items'] = $.cart[getLS('biz')];
					$.oldCart[date.toTimeString()]['biz'] = getLS('biz');
					$.oldCart[date.toTimeString()]['total'] = $('.price').html().split(" ")[1];
					setLS('oldCart',JSON.stringify($.oldCart));
					$.cart[getLS('biz')] = {};
					setLS('cart', JSON.stringify($.cart));
					setLS('lastPickup',pickup);
					$.pickup = moment(pickup).format("hh:mma");
					window.history.pushState(null, null, 'index.html?page=thanks.html');
					loadPage("thanks.html", "slide");
					$.sendNow = 0;
			    },
			    error: function(xhr, errorType, exception) {
			    	$.sendNow = 0;
			    	var errorMessage = exception || xhr.statusText;
				    showError("Error", "There was an error sending your order. Please try again. ("+errorMessage+")", "OK", "sendReceipt");
			    }
			});
		
		}

	}
	
	}

}
	
	
	
function makeLine(l, r, tabs) {
	if (!r) { var r = ''; }
	var max = 26;
	var line = '';
	var out = '';
	if (l.length >= max) {
		st = l.split(' ');
		var i = 0;
		while (i < st.length) {
			if (line.length + st[i].length >= max) {
				out += line + "\n";
				var j = 0;
				while (j < tabs) {
					line += ' ';
					j += 1;
				}
				line = st[i] + " ";
			} else {
				line += st[i] + " ";
			}
			i += 1;
		}
	} else {
		line += l;
	}
	var leftover = max - line.length - r.length;
	if (leftover < 1) {
		line += '\n';
		leftover = max - r.length;
	}
	var i = 1;
	while (i < leftover) {
		line += ' ';
		i += 1;
	}
	line += r;
	
	out += line;
	
	return out + "\n";
}	
	
function wrapLine(string,max) {
	if (string.length >= max) {
		st = string.split(" ");
		var out = '';
		var dump = '';
		var i = 0;
		var j = 0;
		while (i < st.length) {
			if (out.length + st[i].length + 1 >= max) {
				dump += out+"\n";
				out = '   ';
			}
			out += st[i]+" ";
			j += 1;
			i += 1;
		}
		return dump;
	} else {
		return string;
	}
}	
	
	
	
	
	
$('body').on("click", ".immy", function() {
	navigator.camera.getPicture( cameraSuccess, cameraError, {correctOrientation: true, targetWidth: 90, targetHeight:90 });	
});
	
	
function cameraSuccess(data) {
	setLS('userIm',data);
	$('.immy').css({"background-image":"url("+data+")"});
}
	
function cameraError(data) {
}
	
	
	
		
	
	

});


function calcPriceDiffs() {
	$('.subitemgroup').each(function() {
		var x = $(this).find('.picked').parent();
		var worth = parseFloat(x.attr('cartVal'));
		$.each(x.parent().find('.subitemlist'),function() {
			var iam = parseFloat($(this).attr('cartVal'));
			var dos = iam-worth;
			if (dos < 0) {
				dos = "- $"+(dos*-1).toFixed(2);
			} else {
				dos = "+ $"+dos.toFixed(2);
			}
			$(this).find('.toggle_item').html(dos);
		});
		doCart(x.attr('cartCode'), x.attr('cartName'), x.attr('cartVal'),x.attr('cartType'), 'op');
	});
}
	


function goHome(i) {
	setTimeout(function() {
		window.history.pushState(null, null, 'index.html?page=splash.html');
		loadPage("splash.html", "slide");
	},500);
}


function doCart(code,name,val,type,dnum,count) {
	var count = $('.numItems').val();
	if (type == "top") {
		$.cart[getLS('biz')][$.uc]["code"] = code;
		$.cart[getLS('biz')][$.uc]["name"] = name;
		$.cart[getLS('biz')][$.uc]["val"] = val;
		$.cart[getLS('biz')][$.uc]["addThis"] = count;
	} else {
		var  num = 0;
		if (dnum == "op") {
			var num = 1;
		} else if (dnum == -1 && $.cart[getLS('biz')][$.uc]['options'][code]['num']>0) {
			var num = $.cart[getLS('biz')][$.uc]['options'][code]['num']-1;
		} else if (dnum != -1 && $.cart[getLS('biz')][$.uc]['options'][code]) {
			var num = $.cart[getLS('biz')][$.uc]['options'][code]['num']+1;
		} else if (dnum != -1) {
			var num = 1;
		}
		if (num != undefined) {
			$.cart[getLS('biz')][$.uc]['options'][code] = {"code":code, "name": name, "val":val, "num":num};
		}
	}
	
	setLS('cart',JSON.stringify($.cart));
	
	parseCart();

}	


function parseCart() {
	$.cartVal = 0;
	$.cartCount = 0;
	var ctVal = 0;
	$.each($.cart[getLS('biz')], function(p, v) {
		var doVal = 0;
		if (v.options) {
			$.each(v.options, function(op, ov) {
				doVal += (parseFloat(ov.num) * parseFloat(ov.val));
				if (ov.num > 0) {
					$('[uc="' + p + '"] [cartCode="' + op + '"] .remove_item').css({'opacity':1});	
					$('[uc="' + p + '"] [cartCode="' + op + '"] .howmany').html(' x '+ov.num);
				} else {
					$('[uc="' + p + '"] [cartCode="' + op + '"] .remove_item').css({'opacity':0});
					$('[uc="' + p + '"] [cartCode="' + op + '"] .howmany').html('');
				}
			});
		}
		if (v.addThis > 0) {
			//$.cartVal += doVal+parseFloat(v.val);
			$.cartVal += ((doVal+parseFloat(v.val)) * parseFloat(v.addThis));
			$.cartCount += 1;
		}
		$('[uc="' + p + '"] .price').html('$' + ((doVal+parseFloat($('[uc="' + p + '"] .price').attr('basePrice')))).toFixed(2) + ' ea');

		//spit(doVal);
	});
	$('.cartItems').val($.cartCount);
	$('.cartVal').html($.cartVal.toFixed(2));
	if ($.cartCount == 0) {
		$('.summary[href="summary.html"]').hide();
	} else {
		$('.summary[href="summary.html"]').show();
	}
	setLS('cart',JSON.stringify($.cart));
	return $.cartVal.toFixed(2);	
}






	function drawMap(spots, move) {
	
		
		if (lat==undefined) {
			var lat = $.myLat;
		}
		if (lon==undefined) {
			var lon = $.myLon;
		}
		
		if (google === undefined) {
			return false;
		}
		
	
		var map, mapOptions = {}, markerOptions = {}, markersArray = [], 
		len, cx, key, i, bounds;

		mapOptions.center = new google.maps.LatLng(lat, lon);
		mapOptions.zoom = 14;

		
		if (move != 1) {
			mapOptions.disableDoubleClickZoom = true;
			mapOptions.draggable = false;
			mapOptions.scrollwheel = false;
			mapOptions.panControl = false;
		}

		if (Object.size(spots) == 1 && spots['101']) {
			mapOptions.center = new google.maps.LatLng(spots['101'].lat, spots['101'].lon);
			mapOptions.zoom = 17;
		} else {
			$.dobounds = 0;
		}	
		
/*
			$.dobounds = 1;
			markerOptions.bounds = true;
			bounds = new google.maps.LatLngBounds();
*/
		
		
		
		mapOptions.mapTypeId = google.maps.MapTypeId.ROADMAP;
		mapOptions.disableDefaultUI = true;	
	
		$.dmap = new google.maps.Map(document.getElementById('map_canvas'),mapOptions);			
		setInterval(function() {
			google.maps.event.trigger($.dmap, 'resize');		
		}, 100);

		$.each(spots, function(p,v) {
			markerOptions.position = new google.maps.LatLng(v.lat, v.lon);
			
			if ($.dobounds == 1) {
				bounds.extend(markerOptions.position);
				$.dmap.fitBounds(bounds);
			}

			markerOptions.map = $.dmap;
			markerOptions.flat = true;
			markerOptions.title = v.title;
			markerOptions.pane = "floatPane";
			
	        var theMarker = new google.maps.Marker(markerOptions);
	        
			markersArray.push(theMarker);
			
	    });

	    
	    
	}

